package com.joaodjunior.jdjone.ws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.joaodjunior.jdjone.ws.models.Representante;
import com.joaodjunior.jdjone.ws.service.dao.RepresentanteDao;

@Controller
@RequestMapping("login")
public class LoginController {

	@Autowired
	private RepresentanteDao service;
	
	@GetMapping("/novo")
	public ModelAndView showPage() {
		return new ModelAndView("login/login", "representanteDTO", null);
	}
	
	@GetMapping("/{funcionario}")
	public ModelAndView login(@PathVariable Representante representante) {
		if(service.getRepresentante(representante.getId()) != null)
			return new ModelAndView("login/index", "representanteDTO", null);
		else
			return null;
	}
	
}
