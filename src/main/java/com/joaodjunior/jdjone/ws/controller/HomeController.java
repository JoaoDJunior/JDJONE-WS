package com.joaodjunior.jdjone.ws.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.joaodjunior.jdjone.ws.models.Faturamento;
import com.joaodjunior.jdjone.ws.service.dao.FaturamentoDao;

@Controller
@RequestMapping("/") // ficar de olho nessa string
public class HomeController {

	@GetMapping("/")
	public String index() {
		return "index";
	}
	
	@GetMapping("/empresa")
	public String empresa() {
		return "empresa/index";
	}
	
}
