package com.joaodjunior.jdjone.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.joaodjunior.jdjone.ws.models.Analitico;
import com.joaodjunior.jdjone.ws.models.Faturamento;
import com.joaodjunior.jdjone.ws.service.dao.FaturamentoDao;

@RestController
public class FaturamentoController {
	
	@Autowired
	private FaturamentoDao service;
	/*
	@RequestMapping("/analiticos/{analiticoId}/faturamentos")
	public List<Faturamento> getAllFaturamentos(@PathVariable Long analiticoId) {
		return service.getAllFaturamentos(analiticoId);
	}*/
	
	@RequestMapping("/analiticos/{analiticoId}/faturamentos/{id}")
	public Faturamento getFaturamento(@PathVariable Long id) {
		return service.getFaturamento(id);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/analiticos/{analiticoId}/faturamentos")
	public void addFaturamento(@RequestBody Faturamento faturamento, @PathVariable Long analiticoId) {
		//faturamento.setAnalitico(new Analitico(analiticoId, new Integer(0), new Integer(0)));
		service.addFaturamento(faturamento);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/analiticos/{analiticoId}/faturamentos/{id}")
	public void updateFaturamento(@PathVariable Long analiticoId, @PathVariable Long id, @RequestBody Faturamento faturamento) {
		//faturamento.setAnalitico();
		service.updateFaturamento(id, faturamento);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/analiticos/{analiticoId}/faturamentos/{id}")
	public void deleteFaturamento(@PathVariable Long id) {
		service.deleteFaturamento(id);
	}

}
