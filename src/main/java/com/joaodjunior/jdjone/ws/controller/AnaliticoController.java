package com.joaodjunior.jdjone.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.joaodjunior.jdjone.ws.models.Analitico;
import com.joaodjunior.jdjone.ws.service.dao.AnaliticoDao;

@RestController
public class AnaliticoController {
	
	@Autowired
	private AnaliticoDao service;
	
	@RequestMapping("/analiticos")
	public List<Analitico> getAllAnaliticos() {
		return service.getAllAnalitico();
	}
	
	@RequestMapping("/analiticos/{id}")
	public Analitico getAnalitico(@PathVariable Long id) {
		return service.getAnalitico(id);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/analiticos/")
	public void addAnalitico(@RequestBody Analitico analitico) {
		service.addAnalitico(analitico);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/analiticos/{id}")
	public void updateFaturamento(@PathVariable Long id, @RequestBody Analitico analitico) {
		service.updateAnalitico(id, analitico);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/analiticos/{id}")
	public void deleteAnalitico(@PathVariable Long id) {
		service.deleteAnalitico(id);
	}

}
