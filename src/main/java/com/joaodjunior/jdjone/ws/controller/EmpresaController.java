package com.joaodjunior.jdjone.ws.controller;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.POST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.joaodjunior.jdjone.ws.models.Empresa;
import com.joaodjunior.jdjone.ws.service.dao.EmpresaDao;

@Controller
@RequestMapping("/empresas")
public class EmpresaController {
	
	@Autowired
	private EmpresaDao empresaService;
	
	//listar empresas
	@RequestMapping(value="/", method=RequestMethod.GET)
	@ResponseBody
	public ModelAndView getAllEmpresas() {
		List<Empresa> empresas = empresaService.getAllEmpresa();
		return new ModelAndView("empresa/list", "empresas", empresas);
	}
	
	
	@RequestMapping("{id}")
	public ModelAndView view(@PathVariable("id") Long id) {
		return new ModelAndView("empresa/view", "empresa", empresaService.getEmpresa(id));
	}
	@RequestMapping("novo")
	public String createForm(@ModelAttribute Empresa empresa) {
		return "Empresa/form";
	}
	
	@RequestMapping(method=RequestMethod.POST, params="form")
	public ModelAndView create(@Valid Empresa empresa, BindingResult result, RedirectAttributes redirect) {
		if(result.hasErrors()) {
			return new ModelAndView("Form create ","formErrors", result.getAllErrors());
		}
		this.empresaService.addEmpresa(empresa);
		redirect.addFlashAttribute("globalMessage", "Empresa Gravada");
		return new ModelAndView("redirect:/empresa","empresa.id", empresa.getId());
	}
	
	@RequestMapping("alterar/{id}")
	public ModelAndView alterarForm(@PathVariable("id") Empresa empresa) {
		return new ModelAndView("empresa/form","empresa", empresa);
	}
	
	/*
	
	@RequestMapping("/empresas/{cnpj}")
	public Empresa getEmpresa(@PathVariable Long cnpj) {
		return empresaService.getEmpresa(cnpj);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/empresas/")
	public void addEmpresa(@RequestBody Empresa empresaModel) {
		empresaService.addEmpresa(empresaModel);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/empresas/{cnpj}")
	public void updateEmpresa(@PathVariable Long cnpj, @RequestBody Empresa empresa) {
		empresaService.updateEmpresa(cnpj, empresa);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/empresas/{cnpj}")
	public void deleteEmpresa(@PathVariable Long cnpj) {
		empresaService.deleteEmpresa(cnpj);
	}
*/
}
