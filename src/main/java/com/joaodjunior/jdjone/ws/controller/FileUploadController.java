package com.joaodjunior.jdjone.ws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.joaodjunior.jdjone.ws.models.Analitico;
import com.joaodjunior.jdjone.ws.models.Aprovadas;
import com.joaodjunior.jdjone.ws.models.CRLiQ;
import com.joaodjunior.jdjone.ws.models.Empresa;
import com.joaodjunior.jdjone.ws.models.FPD;
import com.joaodjunior.jdjone.ws.models.Faturamento;
import com.joaodjunior.jdjone.ws.models.FaturamentoTotalMensal;
import com.joaodjunior.jdjone.ws.models.RecuperacaoLoja;
import com.joaodjunior.jdjone.ws.models.Representante;
import com.joaodjunior.jdjone.ws.models.StatusLoja;
import com.joaodjunior.jdjone.ws.models.dto.AprovadasDTO;
import com.joaodjunior.jdjone.ws.models.dto.EmpresaDTO;
import com.joaodjunior.jdjone.ws.models.dto.FaturamentoDTO;
import com.joaodjunior.jdjone.ws.models.dto.RecuperacaoDTO;
import com.joaodjunior.jdjone.ws.models.dto.RepresentanteDTO;
import com.joaodjunior.jdjone.ws.models.dto.excel.AnaliticoExcelDto;
import com.joaodjunior.jdjone.ws.service.dao.AnaliticoDao;
import com.joaodjunior.jdjone.ws.service.dao.AprovadasDao;
import com.joaodjunior.jdjone.ws.service.dao.EmpresaDao;
import com.joaodjunior.jdjone.ws.service.dao.FaturamentoDao;
import com.joaodjunior.jdjone.ws.service.dao.FaturamentoTotalMensalDao;
import com.joaodjunior.jdjone.ws.service.dao.RecuperacaoLojaDao;
import com.joaodjunior.jdjone.ws.service.dao.RepresentanteDao;
import com.joaodjunior.jdjone.ws.service.dao.StatusLojaDao;
import com.joaodjunior.jdjone.ws.utils.ExcelWrite;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("api/file")
//@CrossOrigin(origins = "http://localhost:4200")
public class FileUploadController {
	
	@Autowired
	EmpresaDao empresaService;
	@Autowired
	RepresentanteDao representanteService;
	@Autowired
	FaturamentoDao faturamentoService;
	@Autowired
	FaturamentoTotalMensalDao totalMensalService;
	@Autowired
	AnaliticoDao analiticoService;
	@Autowired
	AprovadasDao aprovadaService;
	@Autowired
	RecuperacaoLojaDao recuperacaoService;
	@Autowired
	StatusLojaDao statusService;
    @GetMapping("/")
    public String index() {
        return "upload";
    }

    @PostMapping("/upload")
    public ResponseEntity<Object> singleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) throws IOException {

        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
        }
        
        File arquivo = new File(file.getOriginalFilename());
        OutputStream outputStream = new FileOutputStream(arquivo);
        InputStream input = file.getInputStream();
        int read = 0;
		byte[] bytes = new byte[1024];

		while ((read = input.read(bytes)) != -1) {
			outputStream.write(bytes, 0, read);
		}
		if (file.getInputStream() != null) {
			try {
				file.getInputStream().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (outputStream != null) {
			try {
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
        if (!file.getOriginalFilename().isEmpty()) {
           ExcelWrite ew = new ExcelWrite();
           List<Object> objetos = null;
           try {
        	   objetos = ew.readExcel(arquivo, AnaliticoExcelDto.class);
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
           
           
           Empresa empresa = null;
           Representante representante = null;
           Faturamento faturamento = null;
           FaturamentoTotalMensal totalMensal = null;
           Analitico analitico = null;
           Aprovadas aprovadas = null;
           RecuperacaoLoja recuperacaoLoja = null;
           StatusLoja statusLoja = null;
           FPD fpd = null;
           CRLiQ crLiQ = null;
           
           for(Object object : objetos) {
        	   
        	   AnaliticoExcelDto dto = (AnaliticoExcelDto) object;
        	   
        	   //COLOCAR TODOS OS NOMES EM UPPER CASE PARA FACILITAR CONSULTAS FUTURAS
        	   
        	   representante = new Representante(dto.getGrc().toUpperCase(), dto.getGa().toUpperCase(), dto.getRegionalTricard().toUpperCase(),
        			   dto.getCr().toUpperCase(), dto.getRegionalTribanco().toUpperCase(), dto.getGerenteTribanco().toUpperCase(),
        			   dto.getCidade().toUpperCase(), dto.getEstado().toUpperCase());
        	   
        	   empresa = new Empresa(dto.getCnpj(), dto.getRazaoSocial().toUpperCase(), dto.getNomeFantasia().toUpperCase(), dto.getRede().toUpperCase(),
        			   dto.getCidade().toUpperCase(), dto.getEstado().toUpperCase());
        	   
        	   faturamento = new Faturamento(dto.getFaturamentoPl(), dto.getFaturamentoHib(), dto.getFaturamentoHibOffUs(),
        			   dto.getFaturamentoHibOnUs(), dto.getFaturamentoOnUs(), dto.getFaturamentoDia(), dto.getFaturamentoTotal());
        	   
        	   totalMensal = new FaturamentoTotalMensal(dto.getSegmentoLojista().toUpperCase(), dto.getValorFaturamento());
        	   
        	   aprovadas = new Aprovadas(dto.getEmitidasAcum(), dto.getAprovadas(), dto.getComCompra());
        	   
        	   recuperacaoLoja = new RecuperacaoLoja(dto.getRecuperacao());
        	   
        	   statusLoja = new StatusLoja(dto.getLojaPositiva(), dto.getLojaAprovando(), dto.getLojaAtiva());
   
        	   //1° - Enviar o representante
        	   //|Verificar com o Jessé se esses representantes têm uma matricula
        	   if(representanteService.getRepresentanteByName(representante) == null) {
        		   representanteService.addRepresentante(representante);
        		   empresa.setRepresentante(representante);
        		   empresaService.addEmpresa(empresa);        		   
        	   } else {
        		   if(empresaService.getEmpresaByCnpj(empresa.getCnpj()) == null) {
        			   empresa.setRepresentante(representante);
            		   empresaService.addEmpresa(empresa);
            		   analitico = new Analitico(05, 2018, empresa, faturamento, totalMensal, aprovadas, recuperacaoLoja, statusLoja, null, null);
            		   faturamentoService.addFaturamento(faturamento);
            		   totalMensalService.addTotalMensal(totalMensal);
            		   aprovadaService.addAprovada(aprovadas);
            		   recuperacaoService.addRecuperacaoLoja(recuperacaoLoja);
            		   statusService.addStatusLoja(statusLoja);
            		   analiticoService.addAnalitico(analitico);
        		   }
        	   }
           }
           
        }else{
           return new ResponseEntity<>("Invalid file.",HttpStatus.BAD_REQUEST);
        }
        
        return new ResponseEntity<>("File Uploaded Successfully.",HttpStatus.OK);
    }

    @GetMapping("/uploadStatus")
    public String uploadStatus() {
        return "uploadStatus";
    }
	
}
