package com.joaodjunior.jdjone.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.joaodjunior.jdjone.ws.models.Representante;
import com.joaodjunior.jdjone.ws.service.dao.RepresentanteDao;

@RestController
public class RepresentanteController {
	
	@Autowired
	private RepresentanteDao service;
	
	@RequestMapping("representante")
	public List<Representante> getAllRepresentantes() {
		return service.getAllRepresentantes();
	}
	
	@RequestMapping("representante/{id}")
	public Representante getRepresentante(@PathVariable Long id) {
		return service.getRepresentante(id);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/representante")
	public void addRepresentante(@RequestBody Representante representante) {
		service.addRepresentante(representante);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="representante/{id}")
	public void updateRepresentante(@PathVariable Long id, @RequestBody Representante representante) {
		service.updateRepresentante(id, representante);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="representante/{id}")
	public void deleteRepresentante(@PathVariable Long id) {
		service.deleteRepresentante(id);
	}

}
