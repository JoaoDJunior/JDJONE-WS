package com.joaodjunior.jdjone.ws.repository;

import org.springframework.data.repository.CrudRepository;

import com.joaodjunior.jdjone.ws.models.Cartao;
import com.joaodjunior.jdjone.ws.models.Compra;

public interface CompraRepository extends CrudRepository<Compra, Long> {

}
