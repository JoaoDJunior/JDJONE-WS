package com.joaodjunior.jdjone.ws.repository;

import org.springframework.data.repository.CrudRepository;

import com.joaodjunior.jdjone.ws.models.RecuperacaoLoja;

public interface RecuperacaoLojaRepository extends CrudRepository<RecuperacaoLoja, Long>{

}
