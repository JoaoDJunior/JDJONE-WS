package com.joaodjunior.jdjone.ws.repository;

import org.springframework.data.repository.CrudRepository;

import com.joaodjunior.jdjone.ws.models.CRLiQ;

public interface CRLiQRepository extends CrudRepository<CRLiQ, Long>{

}
