package com.joaodjunior.jdjone.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.joaodjunior.jdjone.ws.models.Analitico;
import com.joaodjunior.jdjone.ws.models.Empresa;

@RepositoryRestResource
@CrossOrigin(origins = "http://localhost:4200")
public interface EmpresaRepository extends CrudRepository<Empresa, Long> {

	@Query("from Analitico where EMPRESA_ID=:empresaId")
	List<Analitico> findAnaliticoByEmpresaId(@Param("empresaId") Long empresaId);
	/*
	public EmpresaModel findByNomeFantasia(String nomeFantasia);
	
	public EmpresaModel findById(Long id);
	
	public List<EmpresaModel> findByRepresentante(Representante representante);
		
	public List<EmpresaModel> selectEmpresas();
	
	public void insertEmpresa(EmpresaModel empresa);
	
	public void updateEmpresa(EmpresaModel empresa);
*/

	Empresa findEmpresaByCnpj(String cnpj);

	Empresa findEmpresaByRede(String rede);

	Empresa findEmpresaByNomeFantasia(String nomeFantasia);
}
