package com.joaodjunior.jdjone.ws.repository;

import org.springframework.data.repository.CrudRepository;

import com.joaodjunior.jdjone.ws.models.StatusLoja;

public interface StatusLojaRepository extends CrudRepository<StatusLoja, Long>{

}
