package com.joaodjunior.jdjone.ws.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.joaodjunior.jdjone.ws.models.LoginAuth;

public interface LoginAuthRepository extends CrudRepository<LoginAuth, Long> {
	
	@Query("from LoginAuth where email=:email and senha=:senha")
	public LoginAuth findByEmailSenha(@Param("email") String email, @Param("senha") String senha);

}
