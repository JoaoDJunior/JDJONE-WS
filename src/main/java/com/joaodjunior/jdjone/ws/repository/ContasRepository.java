package com.joaodjunior.jdjone.ws.repository;

import org.springframework.data.repository.CrudRepository;

import com.joaodjunior.jdjone.ws.models.Contas;

public interface ContasRepository extends CrudRepository<Contas, Long>{

}
