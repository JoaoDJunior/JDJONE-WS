package com.joaodjunior.jdjone.ws.repository;

import org.springframework.data.repository.CrudRepository;

import com.joaodjunior.jdjone.ws.models.Representante;

public interface RepresentanteRepository extends CrudRepository<Representante, Long> {

	Representante findRepresentanteByGrc(String grc);

}
