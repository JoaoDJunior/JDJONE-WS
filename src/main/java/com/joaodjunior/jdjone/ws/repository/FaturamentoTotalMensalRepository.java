package com.joaodjunior.jdjone.ws.repository;

import org.springframework.data.repository.CrudRepository;

import com.joaodjunior.jdjone.ws.models.FaturamentoTotalMensal;

public interface FaturamentoTotalMensalRepository extends CrudRepository<FaturamentoTotalMensal, Long> {

}
