package com.joaodjunior.jdjone.ws.repository;

import org.springframework.data.repository.CrudRepository;

import com.joaodjunior.jdjone.ws.models.SeguroFatura;

public interface SeguroFaturaRepository extends CrudRepository<SeguroFatura, Long>{

}
