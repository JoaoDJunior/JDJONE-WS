package com.joaodjunior.jdjone.ws.repository;

import org.springframework.data.repository.CrudRepository;

import com.joaodjunior.jdjone.ws.models.Analitico;


public interface AnaliticoRepository extends CrudRepository<Analitico, Long> {
	

}
