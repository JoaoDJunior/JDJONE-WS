package com.joaodjunior.jdjone.ws.repository;

import org.springframework.data.repository.CrudRepository;

import com.joaodjunior.jdjone.ws.models.FPD;

public interface FPDRepository extends CrudRepository<FPD, Long> {

}
