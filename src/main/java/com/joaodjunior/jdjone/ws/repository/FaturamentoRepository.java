package com.joaodjunior.jdjone.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.joaodjunior.jdjone.ws.models.Faturamento;

public interface FaturamentoRepository extends CrudRepository<Faturamento, Long> {
	
	//public List<Faturamento> findByAnaliticoId(Long analiticoId);
	
	//public List<Faturamento> findAll(Long idAnalitico);

}
