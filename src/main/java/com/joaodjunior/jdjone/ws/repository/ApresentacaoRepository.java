package com.joaodjunior.jdjone.ws.repository;

import org.springframework.data.repository.CrudRepository;

import com.joaodjunior.jdjone.ws.models.Apresentacao;

public interface ApresentacaoRepository extends CrudRepository<Apresentacao, Long> {
	
	

}
