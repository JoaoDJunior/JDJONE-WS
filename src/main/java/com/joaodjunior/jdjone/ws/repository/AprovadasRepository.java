package com.joaodjunior.jdjone.ws.repository;

import org.springframework.data.repository.CrudRepository;

import com.joaodjunior.jdjone.ws.models.Aprovadas;

public interface AprovadasRepository extends CrudRepository<Aprovadas, Long> {

}
