package com.joaodjunior.jdjone.ws.models.dto;

import java.util.Map;

public class FaturamentoDTO {

	private Long id;
	private double pl;
	private double hib;
	private double fatPl;
	private double fatHib;
	private double delta;
	public FaturamentoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public FaturamentoDTO(Map<String, Object> dto) {
		super();
		this.id =(Long) dto.get("id");
		this.pl =(Double) dto.get("pl");
		this.hib =(Double) dto.get("hib");
		this.fatPl =(Double) dto.get("fatPl");
		this.fatHib =(Double) dto.get("fatHib");
		this.delta =(Double) dto.get("delta");
	}
	public FaturamentoDTO(Long id, double pl, double hib, double fatPl, double fatHib, double delta) {
		super();
		this.id = id;
		this.pl = pl;
		this.hib = hib;
		this.fatPl = fatPl;
		this.fatHib = fatHib;
		this.delta = delta;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public double getPl() {
		return pl;
	}
	public void setPl(double pl) {
		this.pl = pl;
	}
	public double getHib() {
		return hib;
	}
	public void setHib(double hib) {
		this.hib = hib;
	}
	public double getFatPl() {
		return fatPl;
	}
	public void setFatPl(double fatPl) {
		this.fatPl = fatPl;
	}
	public double getFatHib() {
		return fatHib;
	}
	public void setFatHib(double fatHib) {
		this.fatHib = fatHib;
	}
	public double getDelta() {
		return delta;
	}
	public void setDelta(double delta) {
		this.delta = delta;
	}
	
	
}
