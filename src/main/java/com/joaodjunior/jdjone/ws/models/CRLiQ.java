package com.joaodjunior.jdjone.ws.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CRLiQ implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private Long id;
	private double saldo;
	private double extrato;
	private double percentual;
	
	public CRLiQ() {
		super();
	}

	public CRLiQ(Long id, double saldo, double extrato, double percentual) {
		super();
		this.id = id;
		this.saldo = saldo;
		this.extrato = extrato;
		this.percentual = percentual;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public double getExtrato() {
		return extrato;
	}

	public void setExtrato(double extrato) {
		this.extrato = extrato;
	}

	public double getPercentual() {
		return percentual;
	}

	public void setPercentual(double percentual) {
		this.percentual = percentual;
	}

}
