package com.joaodjunior.jdjone.ws.models.dto;

import java.util.Map;

public class AprovadasDTO {

	private Long id;
	private int emitidas;
	private int aprovadas;
	private int aprovadasDefinitivo;
	private int compra;
	private int propostasPendentes;
	private double limiteDefinitivo;
	private double percEmitidas;
	private double deltaAprovadas;
	private double deltaCompras;
	public AprovadasDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AprovadasDTO(Map<String, Object> dto) {
		super();
		this.emitidas =(int) dto.get("emitidas");
		this.aprovadas =(int) dto.get("aprovadas");
		this.aprovadasDefinitivo =(int) dto.get("aprovadasDefinitivo");
		this.compra =(int) dto.get("compra");
		this.propostasPendentes =(int) dto.get("propostasPendentes");
		this.limiteDefinitivo =(Double) dto.get("limiteDefinitivo");
		this.percEmitidas =(Double) dto.get("percEmitidas");
		this.deltaAprovadas =(Double) dto.get("deltaAprovadas");
		this.deltaCompras =(Double) dto.get("deltaCompras");
	}
	public AprovadasDTO(Long id, int emitidas, int aprovadas, int aprovadasDefinitivo, int compra,
			int propostasPendentes, double limiteDefinitivo, double percEmitidas, double deltaAprovadas,
			double deltaCompras) {
		super();
		this.id = id;
		this.emitidas = emitidas;
		this.aprovadas = aprovadas;
		this.aprovadasDefinitivo = aprovadasDefinitivo;
		this.compra = compra;
		this.propostasPendentes = propostasPendentes;
		this.limiteDefinitivo = limiteDefinitivo;
		this.percEmitidas = percEmitidas;
		this.deltaAprovadas = deltaAprovadas;
		this.deltaCompras = deltaCompras;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getEmitidas() {
		return emitidas;
	}
	public void setEmitidas(int emitidas) {
		this.emitidas = emitidas;
	}
	public int getAprovadas() {
		return aprovadas;
	}
	public void setAprovadas(int aprovadas) {
		this.aprovadas = aprovadas;
	}
	public int getAprovadasDefinitivo() {
		return aprovadasDefinitivo;
	}
	public void setAprovadasDefinitivo(int aprovadasDefinitivo) {
		this.aprovadasDefinitivo = aprovadasDefinitivo;
	}
	public int getCompra() {
		return compra;
	}
	public void setCompra(int compra) {
		this.compra = compra;
	}
	public int getPropostasPendentes() {
		return propostasPendentes;
	}
	public void setPropostasPendentes(int propostasPendentes) {
		this.propostasPendentes = propostasPendentes;
	}
	public double getLimiteDefinitivo() {
		return limiteDefinitivo;
	}
	public void setLimiteDefinitivo(double limiteDefinitivo) {
		this.limiteDefinitivo = limiteDefinitivo;
	}
	public double getPercEmitidas() {
		return percEmitidas;
	}
	public void setPercEmitidas(double percEmitidas) {
		this.percEmitidas = percEmitidas;
	}
	public double getDeltaAprovadas() {
		return deltaAprovadas;
	}
	public void setDeltaAprovadas(double deltaAprovadas) {
		this.deltaAprovadas = deltaAprovadas;
	}
	public double getDeltaCompras() {
		return deltaCompras;
	}
	public void setDeltaCompras(double deltaCompras) {
		this.deltaCompras = deltaCompras;
	}
	
}
