package com.joaodjunior.jdjone.ws.models.dto;

import java.util.Map;

public class RepresentanteDTO {
	
	private String grc;
	private String ga;
	private String regional;
	
	public RepresentanteDTO() {
		super();
	}

	public RepresentanteDTO(Map<String, String> dto) {
		super();
		this.grc = dto.get("grc");
		this.ga = dto.get("ga");
		this.regional = dto.get("regional");
	}

	public RepresentanteDTO(String grc, String ga, String regional) {
		super();
		this.grc = grc;
		this.ga = ga;
		this.regional = regional;
	}

	public String getGrc() {
		return grc;
	}

	public void setGrc(String grc) {
		this.grc = grc;
	}

	public String getGa() {
		return ga;
	}

	public void setGa(String ga) {
		this.ga = ga;
	}

	public String getRegional() {
		return regional;
	}

	public void setRegional(String regional) {
		this.regional = regional;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ga == null) ? 0 : ga.hashCode());
		result = prime * result + ((grc == null) ? 0 : grc.hashCode());
		result = prime * result + ((regional == null) ? 0 : regional.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RepresentanteDTO other = (RepresentanteDTO) obj;
		if (ga == null) {
			if (other.ga != null)
				return false;
		} else if (!ga.equals(other.ga))
			return false;
		if (grc == null) {
			if (other.grc != null)
				return false;
		} else if (!grc.equals(other.grc))
			return false;
		if (regional == null) {
			if (other.regional != null)
				return false;
		} else if (!regional.equals(other.regional))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RepresentanteDTO [grc=");
		builder.append(grc);
		builder.append(", ga=");
		builder.append(ga);
		builder.append(", regional=");
		builder.append(regional);
		builder.append("]");
		return builder.toString();
	}
}
