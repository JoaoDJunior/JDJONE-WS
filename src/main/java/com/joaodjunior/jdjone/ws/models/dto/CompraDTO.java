package com.joaodjunior.jdjone.ws.models.dto;

import java.util.Map;

public class CompraDTO {

	private Long id;
	private Double valor;
	public CompraDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CompraDTO(Map<String, Object> dto) {
		super();
		this.id =(Long) dto.get("id");
		this.valor =(Double) dto.get("valor");
	}
	public CompraDTO(Long id, Double valor) {
		super();
		this.id = id;
		this.valor = valor;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	
}
