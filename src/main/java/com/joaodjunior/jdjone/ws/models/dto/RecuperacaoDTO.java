package com.joaodjunior.jdjone.ws.models.dto;

import java.util.Map;

public class RecuperacaoDTO {

	private Long id;
	private double valor;
	public RecuperacaoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public RecuperacaoDTO(Map<String, Object> dto) {
		super();
		this.id =(Long) dto.get("id");
		this.valor =(Double) dto.get("valor");
	}
	public RecuperacaoDTO(Long id, double valor) {
		super();
		this.id = id;
		this.valor = valor;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	
	
}
