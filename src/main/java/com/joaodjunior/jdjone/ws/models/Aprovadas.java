package com.joaodjunior.jdjone.ws.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Aprovadas implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private int qtdEmitidasAcumulada;
	private int qtdAprovadas;
	private int qtdCompra;

	
	public Aprovadas() {
		super();
	}
	
	public Aprovadas(int qtdEmitidasAcumulada, int qtdAprovadas, int qtdCompra) {
		super();
		this.qtdEmitidasAcumulada = qtdEmitidasAcumulada;
		this.qtdAprovadas = qtdAprovadas;
		this.qtdCompra = qtdCompra;
	}

	public Aprovadas(Long id, int qtdEmitidasAcumulada, int qtdAprovadas, int qtdCompra) {
		super();
		this.id = id;
		this.qtdEmitidasAcumulada = qtdEmitidasAcumulada;
		this.qtdAprovadas = qtdAprovadas;
		this.qtdCompra = qtdCompra;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public int getQtdEmitidasAcumulada() {
		return qtdEmitidasAcumulada;
	}


	public void setQtdEmitidasAcumulada(int qtdEmitidasAcumulada) {
		this.qtdEmitidasAcumulada = qtdEmitidasAcumulada;
	}


	public int getQtdAprovadas() {
		return qtdAprovadas;
	}


	public void setQtdAprovadas(int qtdAprovadas) {
		this.qtdAprovadas = qtdAprovadas;
	}


	public int getQtdCompra() {
		return qtdCompra;
	}


	public void setQtdCompra(int qtdCompra) {
		this.qtdCompra = qtdCompra;
	}
	
}
