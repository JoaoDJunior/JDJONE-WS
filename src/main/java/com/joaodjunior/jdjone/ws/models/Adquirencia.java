package com.joaodjunior.jdjone.ws.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Adquirencia implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private Long id;
	private double cielo;
	private double rede;
	private double taxaDebito;
	private double taxaCredito;
	private double taxaVIX;
	private Boolean domicilioMaster;
	private Boolean domicilioVisa;
	private Boolean tefMartins;
	
	public Adquirencia() {
		super();
	}

	public Adquirencia(Long id, double cielo, double rede, double taxaDebito, double taxaCredito, double taxaVIX,
			Boolean domicilioMaster, Boolean domicilioVisa, Boolean tefMartins) {
		super();
		this.id = id;
		this.cielo = cielo;
		this.rede = rede;
		this.taxaDebito = taxaDebito;
		this.taxaCredito = taxaCredito;
		this.taxaVIX = taxaVIX;
		this.domicilioMaster = domicilioMaster;
		this.domicilioVisa = domicilioVisa;
		this.tefMartins = tefMartins;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getCielo() {
		return cielo;
	}

	public void setCielo(double cielo) {
		this.cielo = cielo;
	}

	public double getRede() {
		return rede;
	}

	public void setRede(double rede) {
		this.rede = rede;
	}

	public double getTaxaDebito() {
		return taxaDebito;
	}

	public void setTaxaDebito(double taxaDebito) {
		this.taxaDebito = taxaDebito;
	}

	public double getTaxaCredito() {
		return taxaCredito;
	}

	public void setTaxaCredito(double taxaCredito) {
		this.taxaCredito = taxaCredito;
	}

	public double getTaxaVIX() {
		return taxaVIX;
	}

	public void setTaxaVIX(double taxaVIX) {
		this.taxaVIX = taxaVIX;
	}

	public Boolean getDomicilioMaster() {
		return domicilioMaster;
	}

	public void setDomicilioMaster(Boolean domicilioMaster) {
		this.domicilioMaster = domicilioMaster;
	}

	public Boolean getDomicilioVisa() {
		return domicilioVisa;
	}

	public void setDomicilioVisa(Boolean domicilioVisa) {
		this.domicilioVisa = domicilioVisa;
	}

	public Boolean getTefMartins() {
		return tefMartins;
	}

	public void setTefMartins(Boolean tefMartins) {
		this.tefMartins = tefMartins;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(cielo);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((domicilioMaster == null) ? 0 : domicilioMaster.hashCode());
		result = prime * result + ((domicilioVisa == null) ? 0 : domicilioVisa.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		temp = Double.doubleToLongBits(rede);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(taxaCredito);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(taxaDebito);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(taxaVIX);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((tefMartins == null) ? 0 : tefMartins.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Adquirencia other = (Adquirencia) obj;
		if (Double.doubleToLongBits(cielo) != Double.doubleToLongBits(other.cielo))
			return false;
		if (domicilioMaster == null) {
			if (other.domicilioMaster != null)
				return false;
		} else if (!domicilioMaster.equals(other.domicilioMaster))
			return false;
		if (domicilioVisa == null) {
			if (other.domicilioVisa != null)
				return false;
		} else if (!domicilioVisa.equals(other.domicilioVisa))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (Double.doubleToLongBits(rede) != Double.doubleToLongBits(other.rede))
			return false;
		if (Double.doubleToLongBits(taxaCredito) != Double.doubleToLongBits(other.taxaCredito))
			return false;
		if (Double.doubleToLongBits(taxaDebito) != Double.doubleToLongBits(other.taxaDebito))
			return false;
		if (Double.doubleToLongBits(taxaVIX) != Double.doubleToLongBits(other.taxaVIX))
			return false;
		if (tefMartins == null) {
			if (other.tefMartins != null)
				return false;
		} else if (!tefMartins.equals(other.tefMartins))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Adquirencia [id=");
		builder.append(id);
		builder.append(", cielo=");
		builder.append(cielo);
		builder.append(", rede=");
		builder.append(rede);
		builder.append(", taxaDebito=");
		builder.append(taxaDebito);
		builder.append(", taxaCredito=");
		builder.append(taxaCredito);
		builder.append(", taxaVIX=");
		builder.append(taxaVIX);
		builder.append(", domicilioMaster=");
		builder.append(domicilioMaster);
		builder.append(", domicilioVisa=");
		builder.append(domicilioVisa);
		builder.append(", tefMartins=");
		builder.append(tefMartins);
		builder.append("]");
		return builder.toString();
	}

}
