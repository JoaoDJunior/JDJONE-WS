package com.joaodjunior.jdjone.ws.models.dto;

import java.util.Map;

import com.joaodjunior.jdjone.ws.models.Empresa;

public class EmpresaDTO {
	
	private String cnpj;
	private String nomeFantasia;
	private String razaoSocial;
	private String rede;
	private String cidade;
	private String uf;
	
	public EmpresaDTO() {
		super();
	}
	
	public EmpresaDTO(Map<String, String> objeto) {
		this.cnpj = objeto.get("cnpj");
		this.nomeFantasia = objeto.get("nomeFantasia");
		this.razaoSocial = objeto.get("razaoSocial");
		this.rede = objeto.get("rede");
		this.cidade = objeto.get("cidade");
		this.uf = objeto.get("uf");
	}
	
	public EmpresaDTO(Empresa empresa) {
		this.cnpj = empresa.getCnpj();
		this.nomeFantasia = empresa.getNomeFantasia();
		this.razaoSocial = empresa.getRazaoSocial();
		this.rede = empresa.getRede();
		this.cidade = empresa.getCidade();
		this.uf = empresa.getUf();
	}

	public EmpresaDTO(String cnpj, String nomeFantasia, String razaoSocial, String rede, String cidade, String uf) {
		super();
		this.cnpj = cnpj;
		this.nomeFantasia = nomeFantasia;
		this.razaoSocial = razaoSocial;
		this.rede = rede;
		this.cidade = cidade;
		this.uf = uf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getRede() {
		return rede;
	}

	public void setRede(String rede) {
		this.rede = rede;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cidade == null) ? 0 : cidade.hashCode());
		result = prime * result + ((cnpj == null) ? 0 : cnpj.hashCode());
		result = prime * result + ((nomeFantasia == null) ? 0 : nomeFantasia.hashCode());
		result = prime * result + ((razaoSocial == null) ? 0 : razaoSocial.hashCode());
		result = prime * result + ((rede == null) ? 0 : rede.hashCode());
		result = prime * result + ((uf == null) ? 0 : uf.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmpresaDTO other = (EmpresaDTO) obj;
		if (cidade == null) {
			if (other.cidade != null)
				return false;
		} else if (!cidade.equals(other.cidade))
			return false;
		if (cnpj == null) {
			if (other.cnpj != null)
				return false;
		} else if (!cnpj.equals(other.cnpj))
			return false;
		if (nomeFantasia == null) {
			if (other.nomeFantasia != null)
				return false;
		} else if (!nomeFantasia.equals(other.nomeFantasia))
			return false;
		if (razaoSocial == null) {
			if (other.razaoSocial != null)
				return false;
		} else if (!razaoSocial.equals(other.razaoSocial))
			return false;
		if (rede == null) {
			if (other.rede != null)
				return false;
		} else if (!rede.equals(other.rede))
			return false;
		if (uf == null) {
			if (other.uf != null)
				return false;
		} else if (!uf.equals(other.uf))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmpresaDTO [cnpj=");
		builder.append(cnpj);
		builder.append(", nomeFantasia=");
		builder.append(nomeFantasia);
		builder.append(", razaoSocial=");
		builder.append(razaoSocial);
		builder.append(", rede=");
		builder.append(rede);
		builder.append(", cidade=");
		builder.append(cidade);
		builder.append(", uf=");
		builder.append(uf);
		builder.append("]");
		return builder.toString();
	}
}
