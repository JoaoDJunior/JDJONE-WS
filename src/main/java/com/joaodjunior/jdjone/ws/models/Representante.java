package com.joaodjunior.jdjone.ws.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
public class Representante implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String grc;
	private String ga;
	private String regionalTricard;
	private String acessorAtendimento;
	private String regionalTribanco;
	private String gerenteComercialTribanco;
	private String cidade;
	private String uf;
	
	@OneToMany(mappedBy	="representante",fetch=FetchType.EAGER)
	@Cascade(CascadeType.ALL)
	private List<Empresa> empresas;

	public Representante() {
		super();
	}

	public Representante(Long id, String grc, String ga, String regionalTricard, String acessorAtendimento,
			String regionalTribanco, String gerenteComercialTribanco, String cidade, String uf) {
		super();
		this.id = id;
		this.grc = grc;
		this.ga = ga;
		this.regionalTricard = regionalTricard;
		this.acessorAtendimento = acessorAtendimento;
		this.regionalTribanco = regionalTribanco;
		this.gerenteComercialTribanco = gerenteComercialTribanco;
		this.cidade = cidade;
		this.uf = uf;
	}



	public Representante(String grc, String ga, String regionalTricard, String acessorAtendimento,
			String regionalTribanco, String gerenteComercialTribanco, String cidade, String uf) {
		this.grc = grc;
		this.ga = ga;
		this.regionalTricard = regionalTricard;
		this.acessorAtendimento = acessorAtendimento;
		this.regionalTribanco = regionalTribanco;
		this.gerenteComercialTribanco = gerenteComercialTribanco;
		this.cidade = cidade;
		this.uf = uf;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGrc() {
		return grc;
	}

	public void setGrc(String grc) {
		this.grc = grc;
	}

	public String getGa() {
		return ga;
	}

	public void setGa(String ga) {
		this.ga = ga;
	}

	public String getRegional() {
		return regionalTricard;
	}

	public void setRegional(String regionalTricard) {
		this.regionalTricard = regionalTricard;
	}

	public String getRegionalTricard() {
		return regionalTricard;
	}

	public void setRegionalTricard(String regionalTricard) {
		this.regionalTricard = regionalTricard;
	}

	public String getAcessorAtendimento() {
		return acessorAtendimento;
	}

	public void setAcessorAtendimento(String acessorAtendimento) {
		this.acessorAtendimento = acessorAtendimento;
	}

	public String getRegionalTribanco() {
		return regionalTribanco;
	}

	public void setRegionalTribanco(String regionalTribanco) {
		this.regionalTribanco = regionalTribanco;
	}

	public String getGerenteComercialTribanco() {
		return gerenteComercialTribanco;
	}

	public void setGerenteComercialTribanco(String gerenteComercialTribanco) {
		this.gerenteComercialTribanco = gerenteComercialTribanco;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public List<Empresa> getEmpresas() {
		return empresas;
	}
	
	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}
	
	

}
