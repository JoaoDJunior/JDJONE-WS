package com.joaodjunior.jdjone.ws.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;


@Entity
public class Analitico implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private int mes;
	private int ano;
	
	@ManyToOne
	private Empresa empresa;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_faturamento")
	private Faturamento faturamento;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_faturamentoTotalMensal")
	private FaturamentoTotalMensal faturamentoTotalMensal;
	
	@OneToOne(fetch  = FetchType.EAGER)
	@JoinColumn(name = "id_aprovadas")
	private Aprovadas aprovadas;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_recuperacao")
	private RecuperacaoLoja recuperacao;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_statusLoja")
	private StatusLoja statusLoja;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_fpd")
	private FPD fpd;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_creLiq")
	private CRLiQ creLiq;
	
	public Analitico() {
		super();
	}
	
	public Analitico(Long id, int mes, int ano) {
		super();
		this.id = id;
		this.mes = mes;
		this.ano = ano;
	}

	public Analitico(Long id, int mes, int ano, Empresa empresa) {
		super();
		this.id = id;
		this.mes = mes;
		this.ano = ano;
		this.empresa = empresa;
	}

	public Analitico(int mes, int ano, Empresa empresa, Faturamento faturamento,
			FaturamentoTotalMensal faturamentoTotalMensal, Aprovadas aprovadas, RecuperacaoLoja recuperacao,
			StatusLoja statusLoja, FPD fpd, CRLiQ creLiq) {
		super();
		this.mes = mes;
		this.ano = ano;
		this.empresa = empresa;
		this.faturamento = faturamento;
		this.faturamentoTotalMensal = faturamentoTotalMensal;
		this.aprovadas = aprovadas;
		this.recuperacao = recuperacao;
		this.statusLoja = statusLoja;
		this.fpd = fpd;
		this.creLiq = creLiq;
	}

	public Analitico(int mes, int ano) {
		this.mes = mes;
		this.ano = ano;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Faturamento getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(Faturamento faturamento) {
		this.faturamento = faturamento;
	}

	public Aprovadas getAprovadas() {
		return aprovadas;
	}

	public void setAprovadas(Aprovadas aprovadas) {
		this.aprovadas = aprovadas;
	}

	public RecuperacaoLoja getRecuperacao() {
		return recuperacao;
	}

	public void setRecuperacao(RecuperacaoLoja recuperacao) {
		this.recuperacao = recuperacao;
	}

	public FaturamentoTotalMensal getFaturamentoTotalMensal() {
		return faturamentoTotalMensal;
	}

	public void setFaturamentoTotalMensal(FaturamentoTotalMensal faturamentoTotalMensal) {
		this.faturamentoTotalMensal = faturamentoTotalMensal;
	}

	public StatusLoja getStatusLoja() {
		return statusLoja;
	}

	public void setStatusLoja(StatusLoja statusLoja) {
		this.statusLoja = statusLoja;
	}

	public FPD getFpd() {
		return fpd;
	}

	public void setFpd(FPD fpd) {
		this.fpd = fpd;
	}

	public CRLiQ getCreLiq() {
		return creLiq;
	}

	public void setCreLiq(CRLiQ creLiq) {
		this.creLiq = creLiq;
	}


	
	
}
