package com.joaodjunior.jdjone.ws.models.dto.excel;

import java.util.Map;

public class AnaliticoExcelDto {
	
	private int codigoTricard;
	private String razaoSocial;
	private String nomeFantasia;
	private String cnpj;
	private String produto;
	private String rede;
	private String grc;
	private String ga;
	private String regionalTricard;
	private String cr;
	private String regionalTribanco;
	private String gerenteTribanco;
	private String cidade;
	private String estado;
	private String segmentoLojista;
	private Double valorFaturamento;
	private String canalAtendimento;
	private String redeSmart;
	private int grupoRisco;
	private String statusCentralizacao;
	private Double faturamentoTotal;
	private Double faturamentoDia;
	private int emitidasAcum;
	private int emitidasDia;
	private int aprovadas;
	private int aprovadasDia;
	private int propostasPendentes;
	private int aprovadasLimiteDefinitivo;
	private int comCompra;
	private Double faturamentoHibOffUs;
	private Double faturamentoHibOnUs;
	private Double faturamentoHib;
	private Double faturamentoPl;
	private Double faturamentoOnUs;
	private Double recuperacao;
	private String lojaPositiva;
	private String lojaAprovando;
	private String lojaAtiva;
	
	public AnaliticoExcelDto() {
		super();
	}

	public AnaliticoExcelDto(Map<String, Object> dto) {
		super();
		this.codigoTricard = (int) dto.get("codigoTricard");
		this.razaoSocial = (String) dto.get("razaoSocial");
		this.nomeFantasia = (String) dto.get("nomeFantasia");
		this.cnpj = (String) dto.get("cnpj");
		this.produto = (String) dto.get("produto");
		this.rede = (String) dto.get("rede");
		this.grc = (String) dto.get("grc");
		this.ga = (String) dto.get("ga");
		this.regionalTricard = (String) dto.get("regionalTricard");
		this.cr = (String) dto.get("cr");
		this.regionalTribanco = (String) dto.get("regionalTribanco");
		this.gerenteTribanco = (String) dto.get("gerenteTribanco");
		this.cidade = (String) dto.get("cidade");
		this.estado = (String) dto.get("estado");
		this.segmentoLojista = (String) dto.get("segmentoLojista");
		this.valorFaturamento = (Double) dto.get("valorFaturamento");
		this.canalAtendimento = (String) dto.get("canalAtendimento");
		this.redeSmart = (String) dto.get("redeSmart");
		this.grupoRisco = (int) dto.get("grupoRisco");
		this.statusCentralizacao = (String) dto.get("statusCentralizacao");
		this.faturamentoTotal = (Double) dto.get("faturamentoTotal");
		this.faturamentoDia = (Double) dto.get("faturamentoDia");
		this.emitidasAcum = (int) dto.get("emitidasAcum");
		this.emitidasDia = (int) dto.get("emitidasDia");
		this.aprovadas = (int) dto.get("aprovadas");
		this.aprovadasDia = (int) dto.get("aprovadasDia");
		this.propostasPendentes = (int) dto.get("propostasPendentes");
		this.aprovadasLimiteDefinitivo = (int) dto.get("aprovadasLimiteDefinitivo");
		this.comCompra = (int) dto.get("comCompra");
		this.faturamentoHibOffUs = (Double) dto.get("faturamentoHibOffUs");
		this.faturamentoHibOnUs = (Double) dto.get("faturamentoHibOnUs");
		this.faturamentoHib = (Double) dto.get("faturamentoHib");
		this.faturamentoPl = (Double) dto.get("faturamentoPl");
		this.faturamentoOnUs = (Double) dto.get("faturamentoOnUs");
		this.recuperacao = (Double) dto.get("recuperacao");
		this.lojaPositiva = (String) dto.get("lojaPositiva");
		this.lojaAprovando = (String) dto.get("lojaAprovando");
		this.lojaAtiva = (String) dto.get("lojaAtiva");
	}

	public int getCodigoTricard() {
		return codigoTricard;
	}

	public void setCodigoTricard(int codigoTricard) {
		this.codigoTricard = codigoTricard;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getProduto() {
		return produto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public String getRede() {
		return rede;
	}

	public void setRede(String rede) {
		this.rede = rede;
	}

	public String getGrc() {
		return grc;
	}

	public void setGrc(String grc) {
		this.grc = grc;
	}

	public String getGa() {
		return ga;
	}

	public void setGa(String ga) {
		this.ga = ga;
	}

	public String getRegionalTricard() {
		return regionalTricard;
	}

	public void setRegionalTricard(String regionalTricard) {
		this.regionalTricard = regionalTricard;
	}

	public String getCr() {
		return cr;
	}

	public void setCr(String cr) {
		this.cr = cr;
	}

	public String getRegionalTribanco() {
		return regionalTribanco;
	}

	public void setRegionalTribanco(String regionalTribanco) {
		this.regionalTribanco = regionalTribanco;
	}
	
	public String getGerenteTribanco() {
		return gerenteTribanco;
	}

	public void setGerenteTribanco(String GerenteTribanco) {
		this.gerenteTribanco = gerenteTribanco;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getSegmentoLojista() {
		return segmentoLojista;
	}

	public void setSegmentoLojista(String segmentoLojista) {
		this.segmentoLojista = segmentoLojista;
	}

	public Double getValorFaturamento() {
		return valorFaturamento;
	}

	public void setValorFaturamento(Double valorFaturamento) {
		this.valorFaturamento = valorFaturamento;
	}

	public String getCanalAtendimento() {
		return canalAtendimento;
	}

	public void setCanalAtendimento(String canalAtendimento) {
		this.canalAtendimento = canalAtendimento;
	}

	public String getRedeSmart() {
		return redeSmart;
	}

	public void setRedeSmart(String redeSmart) {
		this.redeSmart = redeSmart;
	}

	public int getGrupoRisco() {
		return grupoRisco;
	}

	public void setGrupoRisco(int grupoRisco) {
		this.grupoRisco = grupoRisco;
	}

	public String getStatusCentralizacao() {
		return statusCentralizacao;
	}

	public void setStatusCentralizacao(String statusCentralizacao) {
		this.statusCentralizacao = statusCentralizacao;
	}

	public Double getFaturamentoTotal() {
		return faturamentoTotal;
	}

	public void setFaturamentoTotal(Double faturamentoTotal) {
		this.faturamentoTotal = faturamentoTotal;
	}

	public Double getFaturamentoDia() {
		return faturamentoDia;
	}

	public void setFaturamentoDia(Double faturamentoDia) {
		this.faturamentoDia = faturamentoDia;
	}

	public int getEmitidasAcum() {
		return emitidasAcum;
	}

	public void setEmitidasAcum(int emitidasAcum) {
		this.emitidasAcum = emitidasAcum;
	}

	public int getEmitidasDia() {
		return emitidasDia;
	}

	public void setEmitidasDia(int emitidasDia) {
		this.emitidasDia = emitidasDia;
	}

	public int getAprovadas() {
		return aprovadas;
	}

	public void setAprovadas(int aprovadas) {
		this.aprovadas = aprovadas;
	}

	public int getAprovadasDia() {
		return aprovadasDia;
	}

	public void setAprovadasDia(int aprovadasDia) {
		this.aprovadasDia = aprovadasDia;
	}

	public int getPropostasPendentes() {
		return propostasPendentes;
	}

	public void setPropostasPendentes(int propostasPendentes) {
		this.propostasPendentes = propostasPendentes;
	}

	public int getAprovadasLimiteDefinitivo() {
		return aprovadasLimiteDefinitivo;
	}

	public void setAprovadasLimiteDefinitivo(int aprovadasLimiteDefinitivo) {
		this.aprovadasLimiteDefinitivo = aprovadasLimiteDefinitivo;
	}

	public int getComCompra() {
		return comCompra;
	}

	public void setComCompra(int comCompra) {
		this.comCompra = comCompra;
	}

	public Double getFaturamentoHibOffUs() {
		return faturamentoHibOffUs;
	}

	public void setFaturamentoHibOffUs(Double faturamentoHibOffUs) {
		this.faturamentoHibOffUs = faturamentoHibOffUs;
	}

	public Double getFaturamentoHibOnUs() {
		return faturamentoHibOnUs;
	}

	public void setFaturamentoHibOnUs(Double faturamentoHibOnUs) {
		this.faturamentoHibOnUs = faturamentoHibOnUs;
	}

	public Double getFaturamentoHib() {
		return faturamentoHib;
	}

	public void setFaturamentoHib(Double faturamentoHib) {
		this.faturamentoHib = faturamentoHib;
	}

	public Double getFaturamentoPl() {
		return faturamentoPl;
	}

	public void setFaturamentoPl(Double faturamentoPl) {
		this.faturamentoPl = faturamentoPl;
	}

	public Double getFaturamentoOnUs() {
		return faturamentoOnUs;
	}

	public void setFaturamentoOnUs(Double faturamentoOnUs) {
		this.faturamentoOnUs = faturamentoOnUs;
	}

	public Double getRecuperacao() {
		return recuperacao;
	}

	public void setRecuperacao(Double recuperacao) {
		this.recuperacao = recuperacao;
	}

	public String getLojaPositiva() {
		return lojaPositiva;
	}

	public void setLojaPositiva(String lojaPositiva) {
		this.lojaPositiva = lojaPositiva;
	}

	public String getLojaAprovando() {
		return lojaAprovando;
	}

	public void setLojaAprovando(String lojaAprovando) {
		this.lojaAprovando = lojaAprovando;
	}

	public String getLojaAtiva() {
		return lojaAtiva;
	}

	public void setLojaAtiva(String lojaAtiva) {
		this.lojaAtiva = lojaAtiva;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + aprovadas;
		result = prime * result + aprovadasDia;
		result = prime * result + aprovadasLimiteDefinitivo;
		result = prime * result + ((canalAtendimento == null) ? 0 : canalAtendimento.hashCode());
		result = prime * result + ((cidade == null) ? 0 : cidade.hashCode());
		result = prime * result + ((cnpj == null) ? 0 : cnpj.hashCode());
		result = prime * result + codigoTricard;
		result = prime * result + comCompra;
		result = prime * result + ((cr == null) ? 0 : cr.hashCode());
		result = prime * result + emitidasAcum;
		result = prime * result + emitidasDia;
		result = prime * result + ((estado == null) ? 0 : estado.hashCode());
		result = prime * result + ((faturamentoDia == null) ? 0 : faturamentoDia.hashCode());
		result = prime * result + ((faturamentoHib == null) ? 0 : faturamentoHib.hashCode());
		result = prime * result + ((faturamentoHibOffUs == null) ? 0 : faturamentoHibOffUs.hashCode());
		result = prime * result + ((faturamentoHibOnUs == null) ? 0 : faturamentoHibOnUs.hashCode());
		result = prime * result + ((faturamentoOnUs == null) ? 0 : faturamentoOnUs.hashCode());
		result = prime * result + ((faturamentoTotal == null) ? 0 : faturamentoTotal.hashCode());
		result = prime * result + ((faturamentoPl == null) ? 0 : faturamentoPl.hashCode());
		result = prime * result + ((ga == null) ? 0 : ga.hashCode());
		result = prime * result + ((grc == null) ? 0 : grc.hashCode());
		result = prime * result + grupoRisco;
		result = prime * result + ((lojaAprovando == null) ? 0 : lojaAprovando.hashCode());
		result = prime * result + ((lojaAtiva == null) ? 0 : lojaAtiva.hashCode());
		result = prime * result + ((lojaPositiva == null) ? 0 : lojaPositiva.hashCode());
		result = prime * result + ((nomeFantasia == null) ? 0 : nomeFantasia.hashCode());
		result = prime * result + ((produto == null) ? 0 : produto.hashCode());
		result = prime * result + propostasPendentes;
		result = prime * result + ((razaoSocial == null) ? 0 : razaoSocial.hashCode());
		result = prime * result + ((recuperacao == null) ? 0 : recuperacao.hashCode());
		result = prime * result + ((rede == null) ? 0 : rede.hashCode());
		result = prime * result + ((redeSmart == null) ? 0 : redeSmart.hashCode());
		result = prime * result + ((regionalTribanco == null) ? 0 : regionalTribanco.hashCode());
		result = prime * result + ((regionalTricard == null) ? 0 : regionalTricard.hashCode());
		result = prime * result + ((segmentoLojista == null) ? 0 : segmentoLojista.hashCode());
		result = prime * result + ((statusCentralizacao == null) ? 0 : statusCentralizacao.hashCode());
		result = prime * result + ((valorFaturamento == null) ? 0 : valorFaturamento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnaliticoExcelDto other = (AnaliticoExcelDto) obj;
		if (aprovadas != other.aprovadas)
			return false;
		if (aprovadasDia != other.aprovadasDia)
			return false;
		if (aprovadasLimiteDefinitivo != other.aprovadasLimiteDefinitivo)
			return false;
		if (canalAtendimento == null) {
			if (other.canalAtendimento != null)
				return false;
		} else if (!canalAtendimento.equals(other.canalAtendimento))
			return false;
		if (cidade == null) {
			if (other.cidade != null)
				return false;
		} else if (!cidade.equals(other.cidade))
			return false;
		if (cnpj == null) {
			if (other.cnpj != null)
				return false;
		} else if (!cnpj.equals(other.cnpj))
			return false;
		if (codigoTricard != other.codigoTricard)
			return false;
		if (comCompra != other.comCompra)
			return false;
		if (cr == null) {
			if (other.cr != null)
				return false;
		} else if (!cr.equals(other.cr))
			return false;
		if (emitidasAcum != other.emitidasAcum)
			return false;
		if (emitidasDia != other.emitidasDia)
			return false;
		if (estado == null) {
			if (other.estado != null)
				return false;
		} else if (!estado.equals(other.estado))
			return false;
		if (faturamentoDia == null) {
			if (other.faturamentoDia != null)
				return false;
		} else if (!faturamentoDia.equals(other.faturamentoDia))
			return false;
		if (faturamentoHib == null) {
			if (other.faturamentoHib != null)
				return false;
		} else if (!faturamentoHib.equals(other.faturamentoHib))
			return false;
		if (faturamentoHibOffUs == null) {
			if (other.faturamentoHibOffUs != null)
				return false;
		} else if (!faturamentoHibOffUs.equals(other.faturamentoHibOffUs))
			return false;
		if (faturamentoHibOnUs == null) {
			if (other.faturamentoHibOnUs != null)
				return false;
		} else if (!faturamentoHibOnUs.equals(other.faturamentoHibOnUs))
			return false;
		if (faturamentoOnUs == null) {
			if (other.faturamentoOnUs != null)
				return false;
		} else if (!faturamentoOnUs.equals(other.faturamentoOnUs))
			return false;
		if (faturamentoTotal == null) {
			if (other.faturamentoTotal != null)
				return false;
		} else if (!faturamentoTotal.equals(other.faturamentoTotal))
			return false;
		if (faturamentoPl == null) {
			if (other.faturamentoPl != null)
				return false;
		} else if (!faturamentoPl.equals(other.faturamentoPl))
			return false;
		if (ga == null) {
			if (other.ga != null)
				return false;
		} else if (!ga.equals(other.ga))
			return false;
		if (grc == null) {
			if (other.grc != null)
				return false;
		} else if (!grc.equals(other.grc))
			return false;
		if (grupoRisco != other.grupoRisco)
			return false;
		if (lojaAprovando == null) {
			if (other.lojaAprovando != null)
				return false;
		} else if (!lojaAprovando.equals(other.lojaAprovando))
			return false;
		if (lojaAtiva == null) {
			if (other.lojaAtiva != null)
				return false;
		} else if (!lojaAtiva.equals(other.lojaAtiva))
			return false;
		if (lojaPositiva == null) {
			if (other.lojaPositiva != null)
				return false;
		} else if (!lojaPositiva.equals(other.lojaPositiva))
			return false;
		if (nomeFantasia == null) {
			if (other.nomeFantasia != null)
				return false;
		} else if (!nomeFantasia.equals(other.nomeFantasia))
			return false;
		if (produto == null) {
			if (other.produto != null)
				return false;
		} else if (!produto.equals(other.produto))
			return false;
		if (propostasPendentes != other.propostasPendentes)
			return false;
		if (razaoSocial == null) {
			if (other.razaoSocial != null)
				return false;
		} else if (!razaoSocial.equals(other.razaoSocial))
			return false;
		if (recuperacao == null) {
			if (other.recuperacao != null)
				return false;
		} else if (!recuperacao.equals(other.recuperacao))
			return false;
		if (rede == null) {
			if (other.rede != null)
				return false;
		} else if (!rede.equals(other.rede))
			return false;
		if (redeSmart == null) {
			if (other.redeSmart != null)
				return false;
		} else if (!redeSmart.equals(other.redeSmart))
			return false;
		if (regionalTribanco == null) {
			if (other.regionalTribanco != null)
				return false;
		} else if (!regionalTribanco.equals(other.regionalTribanco))
			return false;
		if (regionalTricard == null) {
			if (other.regionalTricard != null)
				return false;
		} else if (!regionalTricard.equals(other.regionalTricard))
			return false;
		if (segmentoLojista == null) {
			if (other.segmentoLojista != null)
				return false;
		} else if (!segmentoLojista.equals(other.segmentoLojista))
			return false;
		if (statusCentralizacao == null) {
			if (other.statusCentralizacao != null)
				return false;
		} else if (!statusCentralizacao.equals(other.statusCentralizacao))
			return false;
		if (valorFaturamento == null) {
			if (other.valorFaturamento != null)
				return false;
		} else if (!valorFaturamento.equals(other.valorFaturamento))
			return false;
		return true;
	}
	
	
	
	

}
