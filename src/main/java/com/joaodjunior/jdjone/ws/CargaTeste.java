package com.joaodjunior.jdjone.ws;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.joaodjunior.jdjone.ws.models.Analitico;
import com.joaodjunior.jdjone.ws.models.Empresa;
import com.joaodjunior.jdjone.ws.models.Faturamento;
import com.joaodjunior.jdjone.ws.models.Representante;
import com.joaodjunior.jdjone.ws.service.dao.AnaliticoDao;
import com.joaodjunior.jdjone.ws.service.dao.EmpresaDao;
import com.joaodjunior.jdjone.ws.service.dao.FaturamentoDao;
import com.joaodjunior.jdjone.ws.service.dao.RepresentanteDao;

@Component
public class CargaTeste implements ApplicationRunner {
	
	@Autowired
	private EmpresaDao empresaservice;
	@Autowired
	private RepresentanteDao representanteDao;
	@Autowired
	private FaturamentoDao faturamentoDao;
	@Autowired
	private AnaliticoDao analiticoDao;
	public void run(ApplicationArguments applicationArguments) throws Exception {
		/*List<Empresa> empresas = new ArrayList<>();
		Representante rep = new Representante(new Long(1), "Teste", "Teste", "Teste");
		Empresa empresa = new Empresa(new Long(4), "123456" , "Nome", "Doido", "Teste", "123", "0");
		empresaservice.addEmpresa(empresa);
		empresa.setRepresentante(rep);
		empresas.add(empresa);		
		rep.setEmpresas(empresas);
		representanteDao.addRepresentante(rep);
		
		
		Faturamento faturamento1 = new Faturamento(new Long(1), 20, 30, 40, 50, 60);
		Faturamento faturamento2 = new Faturamento(new Long(2), 20, 30, 40, 50, 60);
		Faturamento faturamento3 = new Faturamento(new Long(3), 20, 30, 40, 50, 60);
		
		faturamentoDao.addFaturamento(faturamento1);
		faturamentoDao.addFaturamento(faturamento2);
		faturamentoDao.addFaturamento(faturamento3);
		
		Analitico analitico1 = new Analitico(new Long(1), 05, 2018, empresa, faturamento1);
		Analitico analitico2 = new Analitico(new Long(2), 06, 2018, empresa, faturamento2);
		Analitico analitico3 = new Analitico(new Long(4), 07, 2018, empresa, faturamento3);
		
		List<Analitico> analiticos = new ArrayList<>();
		analiticos.add(analitico1);
		analiticos.add(analitico2);
		analiticos.add(analitico3);
		
		empresa.setAnaliticos(analiticos);
		
		empresaservice.updateEmpresa(empresa.getId(), empresa);*/
	}

}
