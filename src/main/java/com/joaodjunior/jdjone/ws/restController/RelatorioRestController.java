package com.joaodjunior.jdjone.ws.restController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.joaodjunior.jdjone.ws.service.dao.ApresentacaoDao;

@RestController
@RequestMapping("api")
@CrossOrigin(origins = "http://localhost:4200")
public class RelatorioRestController {
	
	@Autowired
	private ApresentacaoDao service;

}
