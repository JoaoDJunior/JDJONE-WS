package com.joaodjunior.jdjone.ws.restController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.joaodjunior.jdjone.ws.models.Analitico;
import com.joaodjunior.jdjone.ws.models.Faturamento;
import com.joaodjunior.jdjone.ws.service.dao.FaturamentoDao;

@RestController
@RequestMapping("api")
//@CrossOrigin(origins = "http://localhost:4200")
public class FaturamentoRestController {
	
	@Autowired
	private FaturamentoDao service;
	
	@RequestMapping("faturamento")
	public List<Faturamento> getAllFaturamentos() {
		return service.getAllFaturamentos();
	}
	
	@RequestMapping("faturamento/{id}")
	public Faturamento getFaturamento(@PathVariable Long id) {
		return service.getFaturamento(id);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="faturamento")
	public void addFaturamento(@RequestBody Faturamento faturamento) {
		//faturamento.setAnalitico(new Analitico(analiticoId, new Integer(0), new Integer(0)));
		service.addFaturamento(faturamento);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="faturamento/{id}")
	public void updateFaturamento(@PathVariable Long id, @RequestBody Faturamento faturamento) {
		//faturamento.setAnalitico();
		service.updateFaturamento(id, faturamento);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="faturamento/{id}")
	public void deleteFaturamento(@PathVariable Long id) {
		service.deleteFaturamento(id);
	}

}
