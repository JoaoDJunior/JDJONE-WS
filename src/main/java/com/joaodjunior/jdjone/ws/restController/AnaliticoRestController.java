package com.joaodjunior.jdjone.ws.restController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.joaodjunior.jdjone.ws.models.Analitico;
import com.joaodjunior.jdjone.ws.models.Aprovadas;
import com.joaodjunior.jdjone.ws.models.Empresa;
import com.joaodjunior.jdjone.ws.models.Faturamento;
import com.joaodjunior.jdjone.ws.models.RecuperacaoLoja;
import com.joaodjunior.jdjone.ws.service.dao.AnaliticoDao;

@RestController
@RequestMapping("api")
//@CrossOrigin(origins = "http://localhost:4200")
public class AnaliticoRestController {
	
	@Autowired
	private AnaliticoDao service;
	
	@RequestMapping("analitico")
	public List<Analitico> getAllAnaliticos() {
		return service.getAllAnalitico();
	}
	
	@RequestMapping("analitico/{id}")
	public Analitico getAnalitico(@PathVariable Long id) {
		return service.getAnalitico(id);
	}
	
	@RequestMapping("analitico/{id}/empresa")
	public Empresa getEmpresaByAnalitico(@PathVariable Long id) {
		return service.getEmpresa(id);
	}
	
	@RequestMapping("analitico/{id}/faturamento")
	public Faturamento getFaturamentoByAnalitico(@PathVariable Long id) {
		return service.getFaturamento(id);
	}
	
	@RequestMapping("analitico/{id}/aprovada")
	public Aprovadas getAprovadasByAnalitico(@PathVariable Long id) {
		return service.getAprovada(id);
	}
	
	@RequestMapping("analitico/{id}/recuperacaoloja")
	public RecuperacaoLoja getRecuperacaoLojaByAnalitico(@PathVariable Long id) {
		return service.getRecuperacaoLoja(id);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="analitico/")
	public void addAnalitico(@RequestBody Analitico analitico) {
		service.addAnalitico(analitico);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="analitico/{id}")
	public void updateFaturamento(@PathVariable Long id, @RequestBody Analitico analitico) {
		service.updateAnalitico(id, analitico);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="analitico/{id}")
	public void deleteAnalitico(@PathVariable Long id) {
		service.deleteAnalitico(id);
	}

}
