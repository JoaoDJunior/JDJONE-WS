package com.joaodjunior.jdjone.ws.restController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.joaodjunior.jdjone.ws.models.Analitico;
import com.joaodjunior.jdjone.ws.models.Empresa;
import com.joaodjunior.jdjone.ws.service.dao.EmpresaDao;

@RestController
@RequestMapping("api")
//@CrossOrigin(origins = "http://localhost:4200")
public class EmpresaRestController {

	public final EmpresaDao service;
	
	@Autowired
	public EmpresaRestController(EmpresaDao service) {
		this.service = service;
	}
	
	@GetMapping(value="empresa", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<Empresa> buscarEmpresas() {
		return service.getAllEmpresa();
	}
	
	@GetMapping(value="empresa/{empresaId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Empresa buscarEmpresa(@PathVariable("empresaId") Long empresaId) {
		return service.getEmpresa(empresaId);
	}
	
	@GetMapping(value="empresa/{rede}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Empresa buscarEmpresaByRede(@PathVariable("rede") String rede) {
		return service.getEmpresaByRede(rede);
	}
	
	@GetMapping(value="empresa/{nomefantasia}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Empresa buscarEmpresaByNomeFantasia(@PathVariable("nomefantasia") String nomeFantasia) {
		return service.getEmpresaByNomeFantasia(nomeFantasia);
	}
	
	@GetMapping(value="empresa/{empresaId}/analitico", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<Analitico> buscarAnaliticobyEmpresa(@PathVariable("empresaId") Long empresaId) {
		return service.getAnalitico(empresaId);
	}
	
	@PostMapping("empresa")
	public void novaEmpresa(@RequestBody Empresa empresa) {
		service.addEmpresa(empresa);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="empresa/{id}")
	public void updateEmpresa(@PathVariable Long id, @RequestBody Empresa empresa) {
		service.updateEmpresa(id, empresa);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="empresa/{id}")
	public void deleteEmpresa(@PathVariable Long id) {
		service.deleteEmpresa(id);
	}
	
}
