package com.joaodjunior.jdjone.ws.restController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.joaodjunior.jdjone.ws.models.LoginAuth;
import com.joaodjunior.jdjone.ws.service.dao.LoginAuthDao;

@RestController
@RequestMapping("api")
public class LoginAuthRestController {

	@Autowired
	private LoginAuthDao service;
	
	@GetMapping(value="logins", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<LoginAuth> buscarLogins() {
		return service.getAllLogin();
	}
	
	@GetMapping(value="login/{loginId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public LoginAuth buscarLogin(@PathVariable("loginId") Long loginId) {
		return service.getLoginAuth(loginId);
	}
	
	@GetMapping(value="login/{email}/{senha}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public LoginAuth fazerLogin(@PathVariable("email") String email, @PathVariable("senha") String senha) {
		return service.getLogin(email, senha);
	}
	
	@PostMapping("login")
	public void novoLoginAuth(@RequestBody LoginAuth loginAuth) {
		service.addLoginAuth(loginAuth);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="login/{loginId}")
	public void updateLoginAuth(@PathVariable Long loginId, @RequestBody LoginAuth loginAuth) {
		service.atualizarLogin(loginId, loginAuth);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="login/{loginId}")
	public void deleteLoginAuth(@PathVariable Long loginId) {
		service.deleteLogin(loginId);
	}
}
