package com.joaodjunior.jdjone.ws.service.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joaodjunior.jdjone.ws.models.CRLiQ;
import com.joaodjunior.jdjone.ws.repository.CRLiQRepository;

@Service
public class CRLiQDao {

	@Autowired
	private CRLiQRepository repository;
	
	public List<CRLiQ> getAllCRLiQ() {
		List<CRLiQ> liqs = new ArrayList<CRLiQ>();
		repository.findAll().forEach(liqs::add);
		return liqs;
	}
	
	public CRLiQ getCrliqs(Long id) {
		return repository.findOne(id);
	}
	
	public void addCRLIQ(CRLiQ liq) {
		repository.save(liq);
	}
}
