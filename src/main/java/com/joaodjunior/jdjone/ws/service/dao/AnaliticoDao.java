package com.joaodjunior.jdjone.ws.service.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joaodjunior.jdjone.ws.models.Analitico;
import com.joaodjunior.jdjone.ws.models.Aprovadas;
import com.joaodjunior.jdjone.ws.models.Empresa;
import com.joaodjunior.jdjone.ws.models.Faturamento;
import com.joaodjunior.jdjone.ws.models.RecuperacaoLoja;
import com.joaodjunior.jdjone.ws.repository.AnaliticoRepository;

@Service
public class AnaliticoDao {
	
	@Autowired
	private AnaliticoRepository repository;
	
	public List<Analitico> getAllAnalitico() {
		List<Analitico> analiticos = new ArrayList<>();
		repository.findAll()
		.forEach(analiticos::add);
		return analiticos;
	}
	
	public Analitico getAnalitico(Long id) {
		return repository.findOne(id);
	}
	
	public Empresa getEmpresa(Long id) {
		return repository.findOne(id).getEmpresa();
	}

	public void addAnalitico(Analitico analitico) {
		repository.save(analitico);
		
	}

	public void updateAnalitico(Long id, Analitico analitico) {
		repository.save(analitico);
		
	}

	public void deleteAnalitico(Long id) {
		repository.delete(id);
		
	}

	public Faturamento getFaturamento(Long id) {
		return repository.findOne(id).getFaturamento();
	}

	public Aprovadas getAprovada(Long id) {
		return repository.findOne(id).getAprovadas();
	}

	public RecuperacaoLoja getRecuperacaoLoja(Long id) {
		return repository.findOne(id).getRecuperacao();
	}

}
