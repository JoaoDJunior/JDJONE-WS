package com.joaodjunior.jdjone.ws.service.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joaodjunior.jdjone.ws.models.Representante;
import com.joaodjunior.jdjone.ws.repository.RepresentanteRepository;
@Service
public class RepresentanteDao {
	
	@Autowired
	private RepresentanteRepository repository;

	public List<Representante> getAllRepresentantes() {
		List<Representante> representantes = new ArrayList<>();
		repository.findAll()
		.forEach(representantes::add);
		return representantes;
	}

	public Representante getRepresentante(Long id) {
		return repository.findOne(id);
	}
	
	public Representante getRepresentanteByName(Representante representante) {
		return repository.findRepresentanteByGrc(representante.getGrc());
	}

	public void addRepresentante(Representante representante) {
		repository.save(representante);
		
	}

	public void updateRepresentante(Long id, Representante representante) {
		repository.save(representante);
		
	}

	public void deleteRepresentante(Long id) {
		repository.delete(id);
	}

}
