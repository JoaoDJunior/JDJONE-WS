package com.joaodjunior.jdjone.ws.service.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joaodjunior.jdjone.ws.models.Contas;
import com.joaodjunior.jdjone.ws.repository.ContasRepository;

@Service
public class ContasDao {

	@Autowired
	private ContasRepository repository;
	
	public List<Contas> getAllContas() {
		List<Contas> contas = new ArrayList<Contas>();
		repository.findAll().forEach(contas::add);
		return contas;
	}
	
	public Contas getConta(Long id) {
		return repository.findOne(id);
	}
	
	public void addConta(Contas contas) {
		repository.save(contas);
	}
}
