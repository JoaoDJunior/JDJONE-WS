package com.joaodjunior.jdjone.ws.service.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joaodjunior.jdjone.ws.models.FaturamentoTotalMensal;
import com.joaodjunior.jdjone.ws.repository.FaturamentoTotalMensalRepository;

@Service
public class FaturamentoTotalMensalDao {

	@Autowired
	private FaturamentoTotalMensalRepository repository;
	
	public List<FaturamentoTotalMensal> getAllTotalMensal() {
		List<FaturamentoTotalMensal> faturamentos = new ArrayList<FaturamentoTotalMensal>();
		repository.findAll().forEach(faturamentos::add);
		return faturamentos;
	}
	
	public FaturamentoTotalMensal getTotalMensal(Long id) {
		return repository.findOne(id);
	}
	
	public void addTotalMensal(FaturamentoTotalMensal mensal) {
		repository.save(mensal);
	}
	
}
