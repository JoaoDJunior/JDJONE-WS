package com.joaodjunior.jdjone.ws.service.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joaodjunior.jdjone.ws.models.Faturamento;
import com.joaodjunior.jdjone.ws.repository.FaturamentoRepository;
@Service
public class FaturamentoDao {

	
	@Autowired
	private FaturamentoRepository repository;

	public List<Faturamento> getAllFaturamentos() {
		List<Faturamento> faturamentos = new ArrayList<>();
		repository.findAll()
		.forEach(faturamentos::add);
		return faturamentos;
	}
	
	public Faturamento getFaturamento(Long id) {
		return repository.findOne(id);
	}

	public void addFaturamento(Faturamento faturamento) {
		repository.save(faturamento);
		
	}

	public void updateFaturamento(Long id, Faturamento faturamento) {
		repository.save(faturamento);
		
	}

	public void deleteFaturamento(Long id) {
		repository.delete(id);
		
	}
}
