package com.joaodjunior.jdjone.ws.service.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joaodjunior.jdjone.ws.models.RecuperacaoLoja;
import com.joaodjunior.jdjone.ws.repository.RecuperacaoLojaRepository;

@Service
public class RecuperacaoLojaDao {

	@Autowired
	private RecuperacaoLojaRepository repository;
	
	public List<RecuperacaoLoja> getAllRecuperacao() {
		List<RecuperacaoLoja> lojas = new ArrayList<RecuperacaoLoja>();
		repository.findAll().forEach(lojas::add);
		return lojas;
	}
	
	public RecuperacaoLoja getRecuperacao(Long id) {
		return repository.findOne(id);
	}
	
	public void addRecuperacaoLoja(RecuperacaoLoja loja) {
		repository.save(loja);
	}
}
