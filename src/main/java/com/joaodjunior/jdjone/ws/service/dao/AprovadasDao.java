package com.joaodjunior.jdjone.ws.service.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joaodjunior.jdjone.ws.models.Aprovadas;
import com.joaodjunior.jdjone.ws.repository.AprovadasRepository;

@Service
public class AprovadasDao {

	@Autowired
	private AprovadasRepository repository;
	
	public List<Aprovadas> getAllAprovadas() {
		List<Aprovadas> aprovadas = new ArrayList<Aprovadas>();
		repository.findAll().forEach(aprovadas::add);
		return aprovadas; 
	}
	
	public Aprovadas getAprovada(Long id) {
		return repository.findOne(id);
	}
	
	public void addAprovada(Aprovadas aprovadas) {
		repository.save(aprovadas);
	}
}
