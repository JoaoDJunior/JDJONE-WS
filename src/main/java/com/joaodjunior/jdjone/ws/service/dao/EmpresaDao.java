package com.joaodjunior.jdjone.ws.service.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joaodjunior.jdjone.ws.models.Analitico;
import com.joaodjunior.jdjone.ws.models.Empresa;
import com.joaodjunior.jdjone.ws.repository.EmpresaRepository;
@Service
public class EmpresaDao {

	
	@Autowired
	private EmpresaRepository repository;
	
	public List<Empresa> getAllEmpresa() {
		List<Empresa> empresas = new ArrayList<>();
		repository.findAll()
			.forEach(empresas::add);
		return empresas;
	}
	
	public void addEmpresa(Empresa empresa) {
		repository.save(empresa);
	}

	public void updateEmpresa(Long cnpj, Empresa empresa) {
		repository.save(empresa);	
	}

	public Empresa getEmpresa(Long id) {
		return repository.findOne(id);
	}
	
	public Empresa getEmpresaByCnpj(String cnpj) {
		return repository.findEmpresaByCnpj(cnpj);
	}

	public void deleteEmpresa(Long id) {
		repository.delete(id);
		
	}

	public List<Analitico> getAnalitico(Long empresaId) {
		return repository.findAnaliticoByEmpresaId(empresaId);
	}

	public Empresa getEmpresaByRede(String rede) {
		return repository.findEmpresaByRede(rede);
	}
	
	public Empresa getEmpresaByNomeFantasia(String nomeFantasia) {
		return repository.findEmpresaByNomeFantasia(nomeFantasia);
	}
}
