package com.joaodjunior.jdjone.ws.service.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joaodjunior.jdjone.ws.models.Empresa;
import com.joaodjunior.jdjone.ws.models.LoginAuth;
import com.joaodjunior.jdjone.ws.repository.LoginAuthRepository;

@Service
public class LoginAuthDao {
	
	@Autowired
	private LoginAuthRepository repository;
	
	public List<LoginAuth> getAllLogin() {
		List<LoginAuth> logins = new ArrayList<>();
		repository.findAll()
			.forEach(logins::add);
		return logins;
	}
	
	public LoginAuth getLoginAuth(Long id) {
		return repository.findOne(id);
	}
	
	public LoginAuth getLogin(String email, String senha) {
		return repository.findByEmailSenha(email, senha);
	}
	
	public void addLoginAuth(LoginAuth login) {
		repository.save(login);
	}
	
	public void atualizarLogin(Long id, LoginAuth login) {
		repository.save(login);
	}
	
	public void deleteLogin(Long id) {
		repository.delete(id);
	}
	
}
