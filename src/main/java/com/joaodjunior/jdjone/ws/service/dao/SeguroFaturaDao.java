package com.joaodjunior.jdjone.ws.service.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joaodjunior.jdjone.ws.models.SeguroFatura;
import com.joaodjunior.jdjone.ws.repository.SeguroFaturaRepository;

@Service
public class SeguroFaturaDao {

	@Autowired
	private SeguroFaturaRepository repository;
	
	public List<SeguroFatura> getAllSeguros() {
		List<SeguroFatura> faturas = new ArrayList<SeguroFatura>();
		repository.findAll().forEach(faturas::add);
		return faturas;
	}
	
	public SeguroFatura getSeguros(Long id) {
		return repository.findOne(id);
	}
	
	public void addSeguros(SeguroFatura seguro) {
		repository.save(seguro);
	}
}
