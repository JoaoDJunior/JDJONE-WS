package com.joaodjunior.jdjone.ws.service.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joaodjunior.jdjone.ws.models.StatusLoja;
import com.joaodjunior.jdjone.ws.repository.StatusLojaRepository;

@Service
public class StatusLojaDao {

	@Autowired
	private StatusLojaRepository repository;
	
	public List<StatusLoja> getAllStatus() {
		List<StatusLoja> status = new ArrayList<StatusLoja>();
		repository.findAll().forEach(status::add);
		return status;
	}
	
	public StatusLoja getStatus(Long id) {
		return repository.findOne(id);
	}
	
	public void addStatusLoja(StatusLoja status) {
		repository.save(status);
	}
}
