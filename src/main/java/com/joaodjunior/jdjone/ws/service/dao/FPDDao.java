package com.joaodjunior.jdjone.ws.service.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joaodjunior.jdjone.ws.models.FPD;
import com.joaodjunior.jdjone.ws.repository.FPDRepository;

@Service
public class FPDDao {

	@Autowired
	private FPDRepository repository;
	
	public List<FPD> getAllFPDs() {
		List<FPD> fpds = new ArrayList<FPD>();
		repository.findAll().forEach(fpds::add);
		return fpds;
	}
	
	public FPD getFPD(Long id) {
		return repository.findOne(id);
	}
	
	public void addFPDs(FPD fpd) {
		repository.save(fpd);
	}
}
