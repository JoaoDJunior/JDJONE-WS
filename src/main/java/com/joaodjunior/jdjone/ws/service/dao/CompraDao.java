package com.joaodjunior.jdjone.ws.service.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joaodjunior.jdjone.ws.models.Compra;
import com.joaodjunior.jdjone.ws.repository.CompraRepository;

@Service
public class CompraDao {

	@Autowired
	private CompraRepository repository;
	
	public List<Compra> getAllCompras() {
		List<Compra> compras = new ArrayList<Compra>();
		repository.findAll().forEach(compras::add);
		return compras;
	}
	
	public Compra getCompra(Long id) {
		return repository.findOne(id);
	}
	
	public void addCompra(Compra compra) {
		repository.save(compra);
	}
}
