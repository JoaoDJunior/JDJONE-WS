var app = angular.module("login",["checklist-model"],function($locationProvider){
    $locationProvider.html5Mode({
    	  enabled: true,
    	  requireBase: false
   	});
});


app.controller('loginController', function($scope,$location,$http) {
	      
      $scope.fazerLogin = function(funcionario) {
    	  $scope.message ="";
    	  var pedidoStr="";
    	  var prefixo="";
    	  $scope.urllogin="/login/novo"+funcionario;
	      $http.get( $scope.urllogin).success(function (data) {
	        $scope.matricula= data["matricula"];
	        console.log($scope.matricula);
	        $scope.mensagem= data["mensagem"];
	      }).error(function (data, status) {
	        $scope.message = "Aconteceu um problema: " 
	        +"Status:"+ data.status+ " - error:"+data.error;
	      });
	    };
});
