var app = angular.module("empresa",["checklist-model"],function($locationProvider){
    $locationProvider.html5Mode({
    	  enabled: true,
    	  requireBase: false
   	});
});


app.controller('empresaController', function($scope,$location,$http) {

	$scope.empresas = [];
	$scope.subTotal = 0;
	$scope.pedidoItens=[];
	      
	 var carregarEmpresas= function () {
	        $http.get( "/empresas").success(function (data) {
	          $scope.empresas =  data["_embedded"]["empresas"];
	        }).error(function (data, status) {
	          $scope.message = "Aconteceu um problema: " + data;
	        });
	      };
	      
      $scope.fazerPedido = function(pedidoItens) {
    	  $scope.message ="";
    	  var pedidoStr="";
    	  var prefixo="";
    	  for (var i=0; i< $scope.pedidoItens.length; i++) {
   		   pedidoStr+=prefixo+$scope.pedidoItens[i].id;
   		   prefixo=",";
          }
    	  $scope.urlPedido="/rest/pedido/novo/2/"+pedidoStr;
	      $http.get( $scope.urlPedido).success(function (data) {
	        $scope.idPedido= data["pedido"];
	        $scope.mensagem= data["mensagem"];
	        $scope.valorTotal= data["valorTotal"];
	      }).error(function (data, status) {
	        $scope.message = "Aconteceu um problema: " 
	        +"Status:"+ data.status+ " - error:"+data.error;
	      });
	    };

     $scope.isItemSelecionado = function() { 
    	 
    	 if (this.checked)
   		  $scope.subTotal+=this.i.preco;
    	 else
   		  $scope.subTotal-=this.i.preco;    		 

     }

   carregarItens();
	  
});
